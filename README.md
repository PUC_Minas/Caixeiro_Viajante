# Caixeiro viajante

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal, Augusto Vieira e Vinicius Luis Lopes Nunes<br />
**Data:** 2016<br />

### Objetivo

- Implementação do algoritmo de caixeiro viajante nos paradigmas:
  - Força Bruta
  - Branch and Bound
  - Algoritmo Genetico
  - 
### Observação

IDE:  [Code::Blocks](www.codeblocks.org/)<br />
Linguagem: [PHP 7.3](https://php.net/)<br />
Compilador: G++ (GNU)
Banco de dados: Não utiliza<br />
Sistema operacional: Ubuntu
A execução é demorada para as entradas maiores
Todas as execuções geram um arquivo de log de tempo

### Execução
#### Força Bruta
    $ cd Força Bruta
    $ g++ -std=c++11 *.h *.cpp -o TSP_Forca_Bruta
    $ ./TSP_Forca_Bruta < TestInput.in
    
#### Branch and Bound    
    $ cd Branch and Bound    
    $ g++ -std=c++11 *.h *.cpp -o TSP_Branch_and_Bound
    $ ./TSP_Branch_and_Bound < TestInput.in
    
#### Algoritmo Genético    
    $ cd Algoritmo Genético
    $ g++ -std=c++11 *.h *.cpp -o TSP_Algoritmo_Genetico
    $ ./TSP_Algoritmo_Genetico < entrada 75-1000-5
    Outras entradas estão disponiveis para Algoritmo Genetico
    

### Contribuição

Esse projeto está concluído e é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->
