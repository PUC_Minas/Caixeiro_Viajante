#include "Cidade.h"
#include <cmath>

// Construtor vazio
Cidade::Cidade(){}

// Construtor que recebe as coordenadas da cidade
Cidade::Cidade(double in1, double in2){
    x=in1;
    y=in2;
}

// Retorna o valor de x
double Cidade::getX(){
    return x;
}

// Retorna o valor de y
double Cidade::getY(){
    return y;
}

// Seta o valor de x
void Cidade::setX(double in){
    x=in;
}

// Seta o valor de y
void Cidade::setY(double in){
    y=in;
}

// Calcula a distancia euclideana
double Cidade::calculaDistancia(Cidade c1, Cidade c2){
    double out;
    out = sqrt((c1.getX() - c2.getX())*(c1.getX() - c2.getX()) 
			 + (c1.getY() - c2.getY())*(c1.getY() - c2.getY()));
    return out;
}

// Destrutor padrao
Cidade::~Cidade(){}
