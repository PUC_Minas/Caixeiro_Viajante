#ifndef TSP_H
#define TSP_H
#include "Cidade.h"
#include <iostream>
#include <algorithm>
#include <limits>

// Classe para a implementacao da solucao do problema do caixeiro viajante  
// Modalidade: Forca Bruta 
class TSP{
    public:
        TSP( int in );
        int numCidades;
        int* shortestPath;
		// Array dinamico das cidades 
        Cidade* arrayCid; 
		// Funcao que retorna o menor caminho entre as cidades
        double getShortestPath(); 
		// Funcao que mostra na tela o menor caminho
        void showShortestPath();
        ~TSP();
};
#endif // TSP_H
