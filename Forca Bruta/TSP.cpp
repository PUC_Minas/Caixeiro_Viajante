#include "TSP.h"
#define MAX_DOUBLE std::numeric_limits<double>::max()

// Construtor que recebe o numero de cidades
TSP::TSP( int in ){
    numCidades = in;
    shortestPath = new int[ in ];
    arrayCid = new Cidade[ in ];
    for( int i = 0; i < in; i++ ){
        int x, y;
		// Recebe da entrada padrao as coordenadas
        std::cin >> x >> y;
        arrayCid[ i ].setX( x );
        arrayCid[ i ].setY( y );
        arrayCid[ i ].id = i + 1;
    }
}

/** Funcao que retorna o menor caminho entre as cidades listadas em arrayCid
  * Gere todas as permutações entre as cidades
  * Calcule a distancia de cidade a cidade
  */
double TSP::getShortestPath(){
    double out = MAX_DOUBLE;
    double tmp;

	// Gera todos os possiveis caminhos
    do{ 
        tmp = Cidade::calculaDistancia( arrayCid[ 0 ], arrayCid[ numCidades - 1 ] );
        for( int i = 0; i < numCidades-1; i++ ){
            tmp += Cidade::calculaDistancia( arrayCid[ i ], arrayCid[ i + 1 ]);
		}
        if(tmp < out){
            out = tmp;
            for( int i = 0; i < numCidades; i++ )
                shortestPath[ i ] = arrayCid[ i ].id;
        }
    } while( std::next_permutation( arrayCid, arrayCid + numCidades ) );

    return out;
}

// Mostra na tela o menor caminho encontrado
void TSP::showShortestPath(){
    for( int i = 0; i < numCidades; i++ ){
        std::cout << shortestPath[ i ] << " ";
	}
    std::cout << std::endl;
}

// Destrutor padrao
TSP::~TSP(){
    delete[] arrayCid;
    delete[] shortestPath;
}
