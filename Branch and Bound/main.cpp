#include <iostream>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <sys/time.h>
#include "Cidade.h"
#include "TSP.h"

typedef unsigned long long Tempo;

// Retorna o tempo atual
static Tempo getTempo(){
      struct timeval now;
      gettimeofday (&now, NULL);
      return  now.tv_usec + (Tempo)now.tv_sec * 1000000;
}

using namespace std;

// Funcao principal
int main(){
    int in;
    cout << setprecision(2) << fixed;
    fstream file;

    while(cin >> in){
        if(in <= 0) break;
        TSP* obj = new TSP(in);
        file.open("TextLog.txt", ios::out | ios::app);
        cout << endl;
        Tempo t0 = getTempo();
        obj->shortestPath();
        Tempo t1 = getTempo();
		// Salva o tempo para cada caso de teste
        double segs = (t1 - t0) / 1000000.0L;
        cout << "Tempo: " << segs << endl;
        file << "Caso de teste para " << in << " cidades tempo: " << segs << endl;
        file.close();
        delete obj;
    }
    return 0;
}