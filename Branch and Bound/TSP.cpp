#include "TSP.h"
#define MAX_DOUBLE std::numeric_limits<double>::max()

TSP::TSP(int in){
	// Inicializacao
    numCidades=in;
    arrayCid = new Cidade[ numCidades ];
    Cost = new double*[ numCidades ];
    visitArray = new bool[ numCidades ];
    tours = new int*[ numCidades ];
	// Construcao da matriz de adjacencias que representa
	// o grafo
    for(int i=0; i<numCidades; i++){
        double x, y;
        Cost[i] = new double[numCidades];
        tours[i] = new int[numCidades];
        visitArray[i]=false;
		// Recebe cada coordenada do arquivo
        scanf("%lf %lf", &x, &y);
        arrayCid[i].setX(x);
        arrayCid[i].setY(y);
        arrayCid[i].id=i+1;
    }

    // Preenche a matriz de distancias
    for(int i=0; i<numCidades; i++){
        for(int j=0; j<numCidades; j++){
            Cost[i][j]=Cidade::calculaDistancia(arrayCid[i], arrayCid[j]);
        }
	}
}

// Calcula o menor caminho entre todas as cidades
// e seleciona a menor para cada uma
void TSP::shortestPath(){
    int shortestTour;
    double dist=MAX_DOUBLE;
	// Chamada da funcao que calcula o caminho
    for( int i=0; i < numCidades; i++ ){
        double aux = tryPath(i);
		// Testa se a distancia calculada e menor que a atual
        if( aux < dist ){
            dist = aux;
            shortestTour = i;
        }
    }
	// Mostra na tela a menor distancia encontrada
    printf("%lf\n", dist);
    for(int i=0; i < numCidades; i++){
        printf("%d ", tours[ shortestTour ][ i ] + 1 );
	}
    printf("\n");
}

// Chamada da funcao recursiva
double TSP::tryPath(int vert){
    return tryPathRec(vert, true, vert, 0);
}

// Busca a cidade mais proxima, armazena a distancia e vai ate ela
// recursivamente ate nao restar mais cidades, entao retorna a cidade
// inicial soma e salva as distancias calculadas
double TSP::tryPathRec(int vert, bool firstRec, int origin, int current){
    double shortDist=MAX_DOUBLE;
    int p;
	// Se for a primeira chamada recursiva marque todas as cidades como nao visitadas
    if(firstRec){ 
        for(int i=0; i<numCidades; i++){
            visitArray[i]=false;
		}
		// Memorize a cidade de origem
        origin = vert; 
    }
	// Visite a cidade
    visitArray[ vert ] = true; 
	// Armazene a cidade na matriz de possiveis rotas
    tours[ origin ][ current ] = vert; 

    for(int i = 0; i < numCidades; i++){
        if( Cost[ vert ][ i ] != 0 && Cost[ vert ][ i ] < shortDist && !visitArray[ i ]){
            // Encontre a cidade mais proxima
            shortDist=Cost[vert][i];
            p=i;
        }
    }
	// Volte a cidade de origem se nao restam mais cidades
    if( isTheTourComplete() ) return Cost[ vert ][ origin ]; 
	// Visite a cidade mais proxima
    else return shortDist + tryPathRec( p, false, origin, current + 1 ); 

}

// Verifica se existem cidades nao visitadas
bool TSP::isTheTourComplete(){
    bool out=true;

    for(int i=0; i<numCidades; i++){
        out&=visitArray[i];
	}
    return out;
}

// Destrutor padrao
TSP::~TSP(){
    delete[] arrayCid;
    for(int i=0; i<numCidades; i++){

        delete[] Cost[i];
    }
    delete[] Cost;
    delete[] visitArray;
}
