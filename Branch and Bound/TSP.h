#ifndef TSP_H
#define TSP_H
#include "Cidade.h"
#include <cstdio>
#include <vector>
#include <limits>

class TSP{
    public:
        int numCidades;
        double** Cost;
        Cidade* arrayCid;
        bool* visitArray;
        int** tours;
		// Funcoes
        TSP(int in);
        void shortestPath();
        double tryPath(int vert);
        bool isTheTourComplete();
        ~TSP();

    protected:
    private:
        double tryPathRec(int vert, bool firstRec, int origin, int current);
};

#endif // TSP_H
