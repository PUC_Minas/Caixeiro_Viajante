#ifndef CIDADE_H
#define CIDADE_H

// Classe que representa uma cidade e suas coordenadas
class Cidade{
    public:
        Cidade();
        Cidade(double in1, double in2);
        int id;
        virtual ~Cidade();
        double getX();
        double getY();
        void setX(double in);
        void setY(double in);
        static double calculaDistancia(Cidade c1, Cidade c2); /**< Calcula distância entre 2 cidades */
        bool operator < (const Cidade &a) const{
            return id < a.id;
        }

    protected:
    private:
        double x;
        double y;
};
#endif // CIDADE_H
