#include <iostream>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <sys/time.h>
#include "cidade.h"
#include "ga.h"
#include <string>
#include <sstream>

typedef unsigned long long Tempo;

// Retorna o tempo atual
static Tempo getTempo(){
    struct timeval now;
    gettimeofday (&now, NULL);
    return  now.tv_usec + (Tempo)now.tv_sec * 1000000;
}

using namespace std;

// Converte um inteiro para string
string itoss(int i){
    stringstream s;
    s << i;
    return s.str();
}

// Funcao principal
int main(){
    int in, pop, repeticoes, porcentagem;
    cin >> pop >> repeticoes >> porcentagem;

    fstream file;
    string fileName = "TextLog " + itoss(pop) + "-" + itoss(repeticoes) + "-" + itoss(porcentagem) + ".csv";
    file.open( fileName, ios::out | ios::app );
    // Cabecalho para o excel
    file << "Polulacao: " << pop << " - Repeticoes: " << repeticoes << " - Chances de mutacao: " << porcentagem << "%" << endl;
    file << "Quantidade de Cidades;Tempo;Distancia total;Caminho;Mutacoes" << endl;

    while(cin >> in){
        if(in <= 0) break;
        // Cria log de tempo
        Tempo t0 = getTempo();

        genetic *obj = new genetic(in, pop, repeticoes, porcentagem);
        obj->evoluir();

        Tempo t1 = getTempo();
        double segs = (t1 - t0) / 1000000.0L;
        cout<< in <<";" << segs << ";" << obj->getShortestPath() << ";" << obj->showShortestPath() <<";"<<obj->getMutacoes()<<endl;

        // Saida formatada para o CSV
        file << in << ";" << segs << ";" << obj->getShortestPath() << ";" << obj->showShortestPath() << ";" << obj->getMutacoes() << endl;

        delete obj;
    }
    file.close();
    return 0;
}
