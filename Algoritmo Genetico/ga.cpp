#include "ga.h"
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <sstream>

// Construtor vazio
genetic::genetic(){}

// Construtor que recebe a quntidade de cidades, o 
// tamanho da populacao e a chance de mutacao
genetic::genetic(int cities, int pop, int repete, int porcent){
    CIDADES = cities;
    POP_TAM = pop;
    MUT_REP = repete;
    PORCENTAGEM = porcent;
    mutacoes = 0;
    addCidades();
}

// Retorna a chance de mutacao
int genetic::getMutacoes() const{
    return mutacoes;
}

// Adiciona uma cidade
void genetic::addCidades(){
    for(int i = 0 ; i < CIDADES; i++){
        int x,y;
        std::cin >> x >> y;
        vetor_cidades.push_back( new Cidade( x, y) );
    }
}

// Realiza a inicializacao
// Preenche a populacao distribuindo genes aleatoriamente
void genetic::inicializacao(){
    std::vector<int>individuos;

    for(int o = 0; o < POP_TAM; o++){
        // Inicializa a lista de cidade
        for(int i = 0; i < CIDADES  ; i++){
            individuos.push_back(i);
		}
        // Mistura a lista
        for(int i = CIDADES - 1; i > 0; i--) {
            int j = rand() % i;
            std::swap(individuos[i], individuos[j]);
        }
        populacao.push_back( std::pair<double, 
							 std::vector<int>>( calcular( individuos ), 
							 individuos ) );
        individuos.clear();
    }
}

// Mostra na tela as cidades
void genetic::printCidades(){
    for(int i = 0; i < POP_TAM; i++){
        for(auto x: populacao[i].second){
            std::cout<<x<<" ";
        }
        std::cout<<" distancia total "<<populacao[i].first<<std::endl;
    }
}

// Converte inteiro para string
std::string genetic:: itoss(int i){
    std::stringstream s;
    s << i;
    return s.str();
}

// Retorna o menor caminho encontrado
std::string genetic::showShortestPath(){
    std::string out = "";
    if(!populacao.empty()){
        for(auto x : populacao[0].second){
            std::stringstream sgg;
            sgg << x;
            out =  out + sgg.str() + " ";
        }
    }else
        out = "N/A";
    return out;
}

// Seta o mentor caminho 
double genetic::getShortestPath(){
    if(!populacao.empty()){
        return populacao[0].first;
    }else
        return 0.0;
}

// Calcula a distancia das cidades
double genetic::calcular(std::vector<int> vec){
    double dis;

    for(int i = 0; i <= CIDADES-1; i++){
        if(i == CIDADES-1){
            dis=dis+Cidade::calculaDistancia(*vetor_cidades[vec[i]],*vetor_cidades[vec[0]]);
        }else{
            dis=dis+Cidade::calculaDistancia(*vetor_cidades[vec[i]],*vetor_cidades[vec[i+1]]);
        }
    }
    return dis;
}

// Verifica se ja esta no vetor
bool estaNoVetor(int i , std::vector<int> vec ){
    for(auto x : vec){
        if(i==x)
            return true;
    }
    return false;
}

// Cria uma nova populacao a partir dos melhores pais, os
// dois melhores individuos ou seja as menores distancias
void genetic::evolucao(){
    std::pair<double,std::vector<int> > pai = populacao[0];
    std::pair<double,std::vector<int> > mae = populacao[1];
	
	// Popula o filhor com uma faixa de genes (cidades) dos pais copiando
	// para posicoes aleatorias
    populacao.clear();
    for(int i = 0; i<POP_TAM; i++){
        std::vector<int> filhoVec(CIDADES,-1);

        int range1 = rand() % CIDADES;
        int range2 = rand() % (CIDADES+range1)%CIDADES;
        if(range1>range2){
            int aux = range1;
            range1 = range2;
            range2 = aux;
        }
        for(int x = range1; x <= range2; x++){
            filhoVec[x] = pai.second[x];
		}
        for(int x = 0; x < CIDADES; x++){
            if(x>=range1 && x<=range2){
                continue;
            }else{
                int aux = x;
                while(estaNoVetor( mae.second[aux], filhoVec)){
                    aux++;
                    if (aux>=CIDADES){
                        aux = aux%CIDADES;
                    }
                }
                filhoVec[x]=mae.second[aux];
            }
        }
        populacao.push_back(std::pair<double,std::vector<int>>(calcular(filhoVec),filhoVec));
    }
}

// Mutacao da populacao 
// Criterio: Re-insere uma cidade em posicao aleatoria
void genetic::mutacao(){
    for(auto x : populacao){
        int ran1 = rand() % CIDADES;
        int ran2 = rand() % CIDADES;
        while (ran1 == ran2) {
            ran2 = rand() % CIDADES;
        }
        int num = x.second[ran1];
        x.second.emplace(x.second.begin()+ran2,num);
        if (ran1 > ran2)
            ran1++;
        x.second.erase(x.second.begin()+ran1);
    }
}

// Executa o procedimento de chamadas do algoritmo
void genetic::evoluir(){
    inicializacao();
    for(int i = 0; i<MUT_REP ; i++){
        selecao();
        evolucao();
        double val = rand() % 100;
        if(val < (double)PORCENTAGEM/100){
            mutacoes++;
            mutacao();
        }
    }
    selecao();
}

// Retorna se o primeiro par e maior que o segundo
bool ordenaDis(std::pair<double,int> i, std::pair<double,int> j){
    return i.second > j.second;
}

// Retorna se o primeiro e menor que o segundo
bool ordenarPop(std::pair<double, std::vector<int> > i, std::pair<double, std::vector<int> > j){
    return i.first < j.first;
}

// Como o melhor inidividuo e aquele com a menor distancia,
// ordena a populacao de acordo com a menor caminho
void genetic::selecao(){
    std::sort(populacao.begin(),populacao.end(),ordenarPop);
}