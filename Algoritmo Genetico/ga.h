#ifndef GENETIC_H
#define GENETIC_H
#include <vector>
#include <string>
#include "cidade.h"

class genetic{

private:
	// Vetor com as cidades
    std::vector< Cidade* > vetor_cidades; 
	// Vetor com a populacao
    std::vector< std::pair<double, std::vector<int> > > populacao;
	// Tamanho da populacao
    int POP_TAM; 
	// Quantidade de mutacoes
    int MUT_REP; 
	// Quantidade de cidades
    int CIDADES; 
	// Chance de mutacoes
    int PORCENTAGEM; 
    int mutacoes;

	// Recebe a entrada
    void addCidades(); 
	// Mostra todas os individuos da populacao
    void printCidades();
	// Preenche a populacao aleatoreamente
    void inicializacao(); 
	// Calcula distancia das cidades
    double calcular(std::vector<int> vec);
	// Ordena a populacao de acordo com a menor caminho
    void selecao(); 
	// Cria nova populacao a partir dos melhores pais 
	// Dois melhores individuos ou seja menores distancias
    void evolucao();
	// Mutacao da populacao 
	// CRITERIO: Re-insere cidade em posicao aleatoria	
    void mutacao(); 

public:
	// Construtor vazio
    genetic(); 
	// Construtor que recebe quantidade de cidades e tamanho max da população
    genetic(int cities, int pop, int repete, int porcent);
	// Realiza o procedimento de chamadas do algoritmo
    void evoluir();

    // Executar somente ao fim da execucao do algoritmo
	// Retorna distancia total do primeiro individuo da populacao
    double getShortestPath();
	// Mostra o primeiro individuo
    std::string showShortestPath();
    int getMutacoes() const;
    std::string itoss(int i);
};
#endif // GENETIC_H